#!/bin/bash
rc=0
mountroutenfs="$HOME/shared/smx2023.net/exam-ldap-nfs"
mountroutesmb="$HOME/shared/smx2023.net/exam-ldap-smb"
userid=$(getent passwd | grep -w $USER | cut -d: -f3)

logger "testing"

groups | grep -i "gnolls" > /dev/null 2>&1 || rc=1

if ! [ $rc -eq 0 ]; then
	echo "You are not gnolls"
	exit 1
else
	if ! [ -z $mountroutenfs ]; then
		mkdir -p $mountroutenfs
	fi

	if ! [ -d $mountroutesmb]; then
		mkdir -p $mountroutesmb
	fi

	let num=$userid%2
        if [ $num -eq 0 ]; then
		mount -t nfs ldapexam.smx2023.net:/srv/nfs/exam-ldap-nfs $mountroute
	else
		mount -t cifs -o guest //ldapexam.smx2023.net/share $mountroutesmb
	fi
fi

exit 0
