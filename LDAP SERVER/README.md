# Task: OpenLDAP installation

## Slapd Installation

We will install the slapd command.

![](./img/slapd.png)

As password we will type **Lin4dm1n**.

![](./img/password.png)

We will search in the repositories for a tool that allows us to manage the LDAP.

![](./img/search1.png)

## MySQL Server Installation

We will install the mysql-server command.

![](./img/mysql.png)

## Installing phpldapadmin

Installation of the phpldapadmin command to connect through apache to our LDAP.

![](./img/hpldapadmin.png)

To check if everything done so far, we will write:

`http://HERE-MY-IP/phpldapadmin/`

## Reconfiguring slapd

To reconfigure slapd we will have to execute this from the terminal:

``` {.example}
sudo dpkg-reconfigure slapd
```

It will give us the option to skip the configuration, to make our changes we will select the No option.

We will write the name for our DNS domain.

![](./img/dns.png)

We will write the name of the organization.

![](./img/organizacion.png)

Now we will be given the option of whether we want to delete the database, we will indicate **NO**.

![](./img/datos.png)

For the last option we are asked if we want to move the old database, we will indicate again **YES**.

![](./img/datos2.png)

## Configuring phpLDAPadmin

To log in we will type: `cn=admin,dc=ubuntusrv,dc=smx2023,dc=net`.

For the password we will write the previously written one: `Lin4dm1n`.

We will configure the file `/etc/phpldapadmin/config.php` to manage our domain.

![](./img/conf1.png)

## Creating our LDAP objects

We will create a Goblins group by selecting the option: `Generic: Posix Group` in new object.

Within the Goblins group we will create a child called goblins01 by selecting the option: `Generic: User Account`.

Now we will create a group called OU by selecting the option: `Generic: Organizational Unit` as a new object.

![](./img/server.PNG)

## Installation of ldap-utils
To install these services we will execute this in a terminal: `sudo apt-get install ldap-utils`

Now we will be able to see the data of our server by typing this: `ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://HERE-MY-IP`

## Step 2:

### Server configuration:

Installation of the command: `apt-get install gnutls-bin ssl-cert`

To create a private key for the certificate authority:

``` {.example}
certtool --generate-privkey --bits 4096 --outfile /etc/ssl/private/mycakey.pem
```

Create the template/file /etc/ssl/ca.info to define the CA:

``` {.example}
cn = Example Company
ca
cert_signing_key
expiration_days = 3650
```

Create the self-signed CA certificate:

``` {.example}
certtool --generate-self-signed \
--load-privkey /etc/ssl/private/mycakey.pem \
--template /etc/ssl/ca.info \
--outfile /usr/local/share/ca-certificates/mycacert.crt
```

### Next step: `dpkg-reconfigure ca-certificates`

Add the new CA certificate to the trusted list: `update-ca-certificates`

Make a private key for the server:

``` {.example}
certtool --generate-privkey \
--bits 2048 \
--outfile /etc/ldap/ldap01_slapd_key.pem
```

Create the `/etc/ssl/ldap01.info` info file containing:

``` {.example}
organization = Example Company
cn = ldap01.example.com
tls_www_server
encryption_key
signing_key
expiration_days = 365
```

Create the server’s certificate:

``` {.example}
certtool --generate-certificate \
--load-privkey /etc/ldap/ldap01_slapd_key.pem \
--load-ca-certificate /etc/ssl/certs/mycacert.pem \
--load-ca-privkey /etc/ssl/private/mycakey.pem \
--template /etc/ssl/ldap01.info \
--outfile /etc/ldap/ldap01_slapd_cert.pem
```

Now we proceed to give them the following permissions:

`chgrp openldap /etc/ldap/ldap01_slapd_key.pem`
`chmod 0640 /etc/ldap/ldap01_slapd_key.pem`

Create the file certinfo.ldif with the following contents:

``` {.example}
dn: cn=config
add: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/ssl/certs/mycacert.pem
-
add: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ldap/ldap01_slapd_cert.pem
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ldap/ldap01_slapd_key.pem
```

Use the ldapmodify command:

``` {.example}
ldapmodify -Y EXTERNAL -H ldapi:/// -f certinfo.ldif
```

![](./img/ldapmodify.PNG)

In the file /etc/default/slapd and add ldaps:/// in the line: `SLAPD_SERVICES="ldap:/// ldapi:/// ldaps:///"`

Restart service: `systemctl restart slapd`.

`ldapwhoami -x -ZZ -H ldap://ubuntusrv.smx2023.net`

![](./img/ldapwhoami01.PNG)

`ldapwhoami -x -H ldaps://ubuntusrv.smx2023.net`

![](./img/ldapwhoami02.png)

## Client configuration

On the client we will install these commands:

`sudo apt-get install libnss-ldap libpam-ldap ldap-utils`

For the following configuration we will follow the steps:

1. `ldapi:\\ubuntusrv.smx2023.net`
2. `dc=ubuntusrv,dc=smx2023,dc=net`
3. `3`
4. `Yes`
5. `No`
6. `cn=admin,dc=ubuntusrv,dc=smx2023,dc=net`
7. `Lin4dm1n`

![](./img/conf01.png)

![](./img/conf2.png)

![](./img/conf3.png)

On the client we will install these commands:

`apt-get install sssd libpam-sss libnss-sss`

We create a file in the following path: `/etc/sssd/sssd.conf`
Contents of the file:

``` {.example}
[sssd]
services = nss, pam, ifp
config_file_version = 2
domains = smx2023.net

[nss]
filter_groups = root
filter_users = root
reconnection_retries = 3

[domain/smx2023.net]
ldap_id_use_start_tls = True
cache_credentials = True
ldap_search_base = dc=ubuntusrv, dc=smx2023,dc=net
id_provider = ldap
debug_level = 3
auth_provider = ldap
chpass_provider = ldap
access_provider = ldap
ldap_schema = rfc2307
ldap_uri = ldap://ubuntusrv.smx2023.net
ldap_default_bind_dn = cn=admin,dc=ubuntusrv,dc=smx2023,dc=net
ldap_id_use_start_tls = true
ldap_default_authtok = Lin4dm1n
ldap_tls_reqcert = demand
ldap_tls_cacert = /etc/ssl/certs/ldapcacert.crt
ldap_tls_cacertdir = /etc/ssl/certs
ldap_search_timeout = 50
ldap_network_timeout = 60
ldap_access_order = filter
ldap_access_filter = (objectClass=posixAccount)
ldap_user_search_base = cn=goblins,dc=ubuntusrv,dc=smx2023,dc=net
ldap_user_object_class = inetOrgPerson
ldap_user_gecos = cn
enumerate = True
debug_level = 0x3ff0
```

Depending on which port is listening we will use the following commands:

``` {.example}
openssl s_client -connect ubuntusrv.smx2023.net:636 -showcerts < /dev/null | openssl x509 -text | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
```

``` {.example}
openssl s_client -connect ubuntusrv.smx2023.net:389 -starttls ldap -showcerts < /dev/null | openssl x509 -text | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
```

Copy the certificate from BEGIN to END.

Validate the Certificate:

``` {.example}
openssl s_client -connect ubuntusrv.smx2023.net:389 -CAfile /etc/ssl/certs/ldapcacert.crt
```

In the file **/etc/ldap/ldap.conf** look for the line **TLS_CACERT** and add the path to our new validated certificate.

### Permissions in SSSD
The permissions of /etc/sssd/ (files and subfolders) must be:

Owner : root:root

Permissions: 0600

Restart the sssd service:

```
systemctl restart sssd
```

If we get an error, install the command: `apt-get install libsss-simpleifp0`

In the file **/etc/pam.d/common-session** under the line **session optional pam_sss.so** we add:

``` {.example}
session required        pam_mkhomedir.so skel=/etc/skel/ umask=0022
```

## Getent checks

``` {.example}
getent passwd goblin02
```

![](./img/getent.png)

![](./img/user.png)