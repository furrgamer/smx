#!/bin/bash

ou=`ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://192.168.5.138 | grep "ou=" | cut -d"=" -f2 | cut -d"," -f1`
rute="/srv/nfs/invertebrados"

names=`ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://192.168.5.138 | grep -v "ou=" | grep "cn=" | cut -d"=" -f2 | cut -d"," -f1 | grep -v "Invertebrados"`


if ! [ -d $rute ]; then
	mkdir -p $rute
fi

for ous in $ou; do
	if ! [ -d $rute/$ous ]; then
		mkdir -p $rute/$ous
	fi

	if [ "$ous" = "aracnidos" ]; then
		if ! [ -d $rute/$ous/tarantula ]; then
			mkdir -p $rute/$ous/tarantula
		fi
		if ! [ $rute/$ous/viudanegra ]; then
			mkdir -p $rute/$ous/viudanegra
		fi
	elif [ "$ous" = "insectos" ]; then
		if ! [ -d $rute/$ous/cucaracha ]; then
                        mkdir -p $rute/$ous/cucaracha
                fi
                if ! [ $rute/$ous/avispa ]; then
                        mkdir -p $rute/$ous/avispa
                fi
	else
		if ! [ -d $rute/$ous/solitaria ]; then
                        mkdir -p $rute/$ous/solitaria
                fi
                if ! [ $rute/$ous/lombriz ]; then
                        mkdir -p $rute/$ous/lombriz
                fi
	fi
done


exit 0
