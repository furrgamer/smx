#!/bin/bash

filefree="/etc/freeradius/3.0/"

files="client.conf dictionary"

dates=`date +"%d-%m-%y"`
hostapdfiles=`ls /etc/hostapd/ | grep conf`

dnsmasqfiles=`ls /etc/dnsmasq.d/ | grep conf`
if ! [ -d backup ]; then
        mkdir backup
fi


for file in $files; do
        cp $filefree/$file backup/$file
done

if ! [ -e $filefree/mods-enabled/ldap ]; then
        cp $filefree/mods-available/ldap $filefree/mods-enabled/ldap
fi
cp $filefree/sites-enabled/my_server cp backup/my_server
cp $filefree/mods-enabled/ldap backup/ldap

for file in $hostapdfiles; do
        cp /etc/hostapd/$file backup/$file
done

cp /etc/hosts backup/hosts

cp /etc/dnsmasq/dnsmasq.conf backup/dnsmasq.conf

for file in $dnsmasqfiles; do
        cp /etc/dnsmasq.d/$file backup/$file
done

tar -czvf backup_$dates.tar.gz backup

fi
exit 0
