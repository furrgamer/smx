# FREERADIUS

## Paso 1

## Freeradius Installation

``` {.example}
apt install freeradius freeradius-ldap apache2
systemctl enable --now apache2
```

``` {.example}
apt install php libapache2-mod-php php-{gd,common,mail,mail-mime,mysql,pear,db,mbstring,xml,curl}
```

## Paso 2

Habilitar cortafuegos:

``` {.example}
ufw allow Apache
```

## Paso 3

Crear carpeta backup, añadimos los ficheros de copia y los eliminaremos de su ruta original.

``` {.example}
mkdir backup
cp /etc/freeradius/3.0/sites-enabled/default backup/ && cp /etc/freeradius/3.0/sites-enabled/inner-tunnel backup/
rm /etc/freeradius/3.0/sites-enabled/default && rm /etc/freeradius/3.0/sites-enabled/inner-tunnel
```

## Paso 4

Creamos este usuario:

``` {.example}
nano /etc/freeradius/3.0/sites-enabled/my_server
```

Añadimos este contenido:

``` {.example}
server my_server {
listen {
        type = auth
        ipaddr = *
        port = 1812
}
authorize {
        ldap
        if (ok || updated)  {
        update control {
        Auth-Type := ldap
        }
        }
}
authenticate {
        Auth-Type LDAP {
                ldap
        }
}
}
```

## Paso 5

Habilitamos el fichero ldap

``` {.example}
cp /etc/freeradius/3.0/mods-available/ldap /etc/freeradius/3.0/mods-enabled/
```

``` {.example}
ldap {
    server = 'ubuntusrv.smx2023.net'
    identity = 'cn=admin,dc=ubuntusrv,dc=smx2023,dc=net'
    password = 1
    base_dn = 'dc=ubuntusrv,dc=smx2023,dc=net'
    user {
        base_dn = "${..base_dn}"
        filter = "(cn=%{%{Stripped-User-Name}:-%{User-Name}})"
        scope = 'sub'
    }
    group {
        base_dn = 'cn=goblins,dc=ubuntusrv,dc=smx2023,dc=net'
        filter = '(objectClass=groupOfUniqueNames)'
        membership_attribute = "memberOf"
        scope = 'sub'
    }
}
```

## Paso 6

Añadimos esto en el fichero /etc/freeradius/3.0/clients.conf

``` {.example}
client cisco_router {
    ipaddr = 192.168.0.17
    secret = testing123
    require_message_authenticator = yes
}
```

## Paso 7

Ejecutamos freeradius -X

``` {.example}
freeradius -X
```

Si todo ha salido bien, en otra terminal ejecutaremos:

``` {.exampke}
radtest goblin01 1 127.0.0.1 1812 testing123
```

## Paso 8

Añadir en el fichero /etc/freeradius/3.0/dictionary

``` {.example}
ATTRIBUTE My_Group 3000 string
```

Install hostapd

``` {.example}
apt install hostapd
```

Configurar fichero /etc/hostapd/hostapd.conf

``` {.example}
interface=wlxac15a2af25cf
driver=nl80211
ssid=barghest
hw_mode=g
channel=6
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=3
wpa_passphrase=12345678
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

Reiniciamos servicio

``` {.example}
service dnsmask restart
```

Configuracion netplan: /etc/netplan/01-network-manager-all.yaml

``` {.example}
# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: yes
  wifis:
    wlxac15a2af25cf:
      dhcp4: no
      access-points:
        "barghest":
                  password: "12345678"
      addresses: [192.168.175.2/24]
      routes:
      -  to: default
         via: 192.168.175.254
```

Ejecutamos 

``` {.example}
netplan try && netplan apply
```

Ahora en el fichero /etc/default/hostapd añadimos: DAEMON_CONF=/etc/hostapd/hostapd.conf

Instalamos dnsmasq

``` {.example}
apt install dnsmasq
```

Descomentamos en el fichero /etc/dnsmasq.conf esta linea: conf-dir=/etc/dnsmasq.d/,*.conf

Creamos un fichero en  /etc/dnsmasq.d/ llamado radius.conf

``` {.example}
interface=wlxac15a2af25cf
dhcp-range=192.168.175.2,192.168.175.254,12h
listen-address=::1,127.0.0.1
server=8.8.8.8
cache-size=1000
```

Añadimos esta regla de iptables:

``` {.example}
iptables -A PREROUTING -i wlxac15a2af25cf -p tcp -j DNAT --to 127.0.0.1 
iptables -t nat -A POSTROUTING -o enp0s3 -d 127.0.0.1 -p tcp -j SNAT
```
