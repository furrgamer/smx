#!/bin/bash
dir="todos lan"
for r in $dir; do
	rute=`df -h | grep -w "/client/$r" | tr -s " " | cut -d" " -f6`
	if [ -z "$rute" ]; then
		if [ "$r" = "todos" ]; then
			echo "Se ha montado el directorio /client/todos" > $HOME/file.txt
			logger -e -f $HOME/file.txt
			sudo mount 192.168.45.10:/srv/nfs/exportados-todos /client/todos
		elif [ "$r" = "lan" ]; then
			echo "Se ha montado el directorio /client/lan" > $HOME/file2.txt
			logger -e -f $HOME/file2.txt
			sudo mount 192.168.45.10:/srv/nfs/exportados-lan /client/lan
		fi

	else
		echo "$rute esta montado."
	fi
done

for d in $dir; do
	ruta=`cat /etc/fstab | grep "/exportados-$d"`
	echo $ruta
	if [ -z "$ruta" ]; then
		if [ "$d" = "todos" ]; then
			sudo echo "192.168.45.10:/srv/nfs/exportados-todos	/client/todos	nfs	default.auto	0	0" >> /etc/fstab
		elif [ "$d" = "lan" ]; then
			sudo echo "192.168.45.10:/srv/nfs/exportados-lan	/client/lan	nfs	default.auto	0	0" >> /etc/fstab
		fi
	fi
	sudo mount -a
done
exit 0
