#!/bin/bash

#Delete file provided by Senia
#rm -rf arp.log
#Download the file provided by Senia
#wget -q tic.ieslasenia.org/arp.log
#Infinite loop that does not execute the error search until the ping error is received.
while true; do
	p=`ping -c5 10.239.3.7 | tail -n2 | head -n1 | cut -d" " -f6 | cut -d"%" -f1`
	if ! [ $p -eq 0 ]; then
		break
	fi
done
#We read the original file only by collecting a filtering of the non-repeated macs.
cat arp.log | cut -d" " -f5 | sort -u | while read line; do
	for i in $line; do
#	Delete the file generated from the macs
	rm -rf macdeleted.txt
#	We perform a search in the original file and save it in another file.
	cat arp.log | grep -v "FAILED\|INCOMPLETE" | grep $i  >> macdeleted.txt
#	We pick up the first line
	l=`cat macdeleted.txt | sed -n '1p'`
#	We count the characters in the line
	cont=`cat macdeleted.txt |grep -c "$l"`
#	We count the lines
	lineas=`cat macdeleted.txt | wc -l`
#	If the values aren't equal then they are wrong
	if ! [ $cont -eq $lineas ]; then
		Ips=`cat macdeleted.txt | cut -d" " -f1 | sort -u`
		echo "########################"
		echo "#	       (Error)       #"
		echo "#  Ips: $Ips tiene como mac $i  #"
	fi
	done
done
rm -rf macdeleted.txt
exit 0
