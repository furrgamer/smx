#!/bin/bash

user="piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor"
groups="strings woodwind metalwind percussion conductor orchestra"

for g in $groups; do
	grups=`cat /etc/group | cut -d":" -f1 | grep -w "$g" `
	if ! [ "$g" = "$grups" ]; then
		sudo groupadd "$g"
	fi
done

for u in $user; do
	usuar=`cat /etc/passwd | cut -d":" -f1 | grep -w "$u"`
	if ! [ "$u" = "$usuar" ]; then
		if [ "$u" = "piccolo" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "woodwind,orchestra" -s /bin/bash
		elif [ "$u" = "clarinet" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "woodwind,orchestra" -s /bin/bash
		elif [ "$u" = "horn" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "metalwind,orchestra" -s /bin/bash
		elif [ "$u" = "trunk" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "metalwind,orchestra" -s /bin/bash
		elif [ "$u" = "fiddle" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "strings,orchestra" -s /bin/bash
		elif [ "$u" = "viola" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "strings,orchestra" -s /bin/bash
		elif [ "$u" = "cello" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "strings,orchestra" -s /bin/bash
		elif [ "$u" = "doublebass" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "strings,orchestra" -s /bin/bash
		elif [ "$u" = "battery" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "percussion,orchestra" -s /bin/bash
		elif [ "$u" = "xylophone" ]; then
			useradd -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "percussion,orchestra" -s /bin/bash
		elif [ "$u" = "conductor" ]; then
			useradd -N -m "$u" -p $(echo "$u" | openssl passwd -1 -stdin) -G "conductor,orchestra" -s /bin/bash
		fi
	fi
done

dir="TheGreatGateOfKiev BlueDanube NewWorldSymphonie TheJazzSuite"
rute="/srv/sox"

if ! [ -d $rute ]; then
	mkdir -p $rute
fi

for d in $dir; do
	if ! [ -d "$rute/$d" ]; then
		mkdir -p "$rute/$d"
		cd "$rute/$d"
		for f in $user; do
			if ! [ -f "$f" ]; then
				echo "This is the instrument: $f" > $f.txt
				chown $f $f.txt
				chmod u+rw $f.txt
				chmod o-r $f.txt
			fi
		done
		cd ..
	fi
done

for d in $dir; do
	cd "$rute/$d"
	for g in $groups; do
		if [ "$g" = "strings" ]; then
			chown :$g fiddle.txt viola.txt cello.txt doublebass.txt
			chmod g+r fiddle.txt viola.txt cello.txt doublebass.txt
		elif [ "$g" = "woodwind" ]; then
			chown :$g piccolo.txt clarinet.txt
			chmod g+r piccolo.txt clarinet.txt
		elif [ "$g" = "metalwind" ]; then
			chown :$g horn.txt trunk.txt
			chmod g+r horn.txt trunk.txt
		elif [ "$g" = "percussion" ]; then
			chown :$g battery.txt xylophone.txt
			chmod g+r battery.txt xylophone.txt
		elif [ "$g" = "conductor" ]; then
			chown :$g conductor.txt
			setfacl -R -m u:$g:rwx "$rute/$d"
			chmod g+rwx ../*
		fi
	done
	cd ..
done

exit 0
