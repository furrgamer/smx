#!/bin/bash

user="piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor"
groups="strings woodwind metalwind percussion conductor orchestra"
dir="Gallop Carmina 1812"
rute="/srv/sox"

if ! [ -d $rute ]; then
	mkdir -p $rute
fi

for d in $dir; do
	if ! [ -d "$rute/$d" ]; then
		mkdir -p "$rute/$d"
		cd "$rute/$d"
		for f in $user; do
			if ! [ -f "$f.txt" ]; then
				echo "This is the instrument: $f" > $f.txt
				chown $f $f.txt
				chmod u+rw $f.txt
                chmod o-r $f.txt
			fi
		done

        for f in $user; do
            if [ "$d" = "Gallop" ]; then
                users=`cat /etc/group | cut -d":" -f4 | grep -w "strings"`
                echo $users | tee cello.txt viola.txt fiddle.txt doublebass.txt >> /dev/null
            elif [ "$d" = "Carmina" ]; then
                ls -t $rute/$d >> conductor.txt
            elif [ "$d" = "1812" ]; then
                HISTFILE=/home/$f/.bash_history
                HISTTIMEFORMAT='%F %T'
                set -o history
                history >> $f.txt
            fi
        done
		cd ..
	fi
done

for d in $dir; do
	cd "$rute/$d"
	for g in $groups; do
		if [ "$g" = "strings" ]; then
			chown :$g fiddle.txt viola.txt cello.txt doublebass.txt
			chmod g+r fiddle.txt viola.txt cello.txt doublebass.txt
		elif [ "$g" = "woodwind" ]; then
			chown :$g piccolo.txt clarinet.txt
			chmod g+r piccolo.txt clarinet.txt
		elif [ "$g" = "metalwind" ]; then
			chown :$g horn.txt trunk.txt
			chmod g+r horn.txt trunk.txt
		elif [ "$g" = "percussion" ]; then
			chown :$g battery.txt xylophone.txt
			chmod g+r battery.txt xylophone.txt
		elif [ "$g" = "conductor" ]; then
			chown :$g conductor.txt
			setfacl -R -m u:$g:rwx "$rute/$d"
			chmod g+rwx ../*
		fi
	done
	cd ..
done

exit 0