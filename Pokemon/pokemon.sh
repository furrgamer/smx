#!/bin/bash

if ! [ -e chart.csv ]; then
	wget -q https://raw.githubusercontent.com/zonination/pokemon-chart/master/chart.csv
fi

if ! [ $# -eq 2 ]; then
	echo "Error: Argumentos invalidos"
	exit 1
fi

typeD=`cat chart.csv | sed -n '1p' | tr -s "," " "`

numB=0

for a in $typeD; do
	if ! [ "$a" = "$2" ]; then
		let numB=numB+1
	elif [ "$a" = "$2" ]; then
		let numB=numB+1
		result=`cat chart.csv | cut -d"," -f1,$numB | grep -w "$1" | cut -d"," -f2`
		echo "El tipo $1 le hace el tipo $2: $result"
	fi
done
exit 0
