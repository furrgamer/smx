#!/bin/bash
#Primer argumento numero de uvas
uva=$1
#multiplico el numero de uvas x 2
let n=$uva*2
#multiplico el resultado de n x 4 + 1
let a=$n*4+1
s=`echo "scale=2;sqrt ($a)" | bc`
decimal=`echo $s | cut -d"." -f2`
if [ "$decimal" != "00" ]; then
	s=`echo "sqrt ($a)" | bc`
	let d=($s-1)/2
	let result=$d+1
	echo $result
else
	s=`echo "sqrt ($a)" | bc`
	let d=($s-1)/2
	echo $d
fi
exit 0
