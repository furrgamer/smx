#!/bin/bash

hora=`ps -ex | tr -s " " | cut -d" " -f5 | grep -v "TIME"`
dias=0

for line in $hora; do

	h=`echo $line | cut -d":" -f1 | bc`
	s=`echo $line | cut -d":" -f2 | bc`
	let hour=$h+hour
	let second=$s+second
	if [ $second -gt 60 ]; then
		let second=second-60
		let hour=hour+1
	fi

	if [ $hour -gt 24 ]; then
		let dias=dias+1
		let hour=hour-24
	fi
done
if [ $hour -lt 10 ]; then
	if [ $second -lt 10 ]; then
		echo "Dias: $dias, 0$hour:0$second"
	else
		echo "Dias: $dias, 0$hour:$second"
	fi
else
	if [ $second -lt 10 ]; then
		echo "Dias: $dias, $hour:0$second"
	else
		echo "Dias: $dias, $hour:$second"
	fi
fi
exit 0
