#!/bin/bash

fecha=`date +"%Y%m%d-%H%M"`
user="$1"
dir="/home/$user/SolarSystem"
system="Mercury Venus Mars Jupiter Saturn Uranus Neptune"
if [ $# -le 3 ]; then

	userconf=`cat /etc/group | grep -w "$1" | cut -d":" -f1`
	if [ "$user" = "$userconf" ]; then
		if [ "$2" = "replenish" ]; then

			if ! [ -d $dir ]; then
				mkdir -p $dir
			fi

			cd $dir

			for d in $system; do
				if ! [ -d $d ]; then
					mkdir $d
				fi

				cd $d

				if ! [ "$d" = "Saturn" ]; then
					touch control-planet.txt
				else
					if ! [ -z "$3" ]; then
						touch Environment-$user-$fecha.txt
					fi
				fi

				cd ..

			done

		elif [ "$2" = "test" ]; then
			file="$3"
			rc=0

			if [ -d "$file" ]; then
				direct=`ls -l "$file" | tr -s " " | cut -d" " -f9`
				for l in $direct; do
				find "$file/$l" -not -path "*/.*" -perm 400 -user "$user" >> /dev/null 2>&1 || rc=1
					if ! [ -z "$direct" ]; then
						if ! [ $rc -eq 1 ]; then
							echo "The user has read permissions on: $file/$l"
						else
							echo "The user does not have read permissions in: $file/$l"
						fi
					fi
				done
			else
				find /home/$user/SolarSystem -name "$file" -perm 400 -user "$user" >> /dev/null 2>&1 || rc=1
				if ! [ $rc -eq 1 ]; then
					echo "The user has read permissions on: $file"
				else
					echo "The user does not have read permissions in: $file"
				fi
			fi
		elif [ "$2" = "clean" ]; then
			file=$3
			rc=0
			find /home/$user/SolarSystem -name "$file" -perm 400 -user "$user" >> /dev/null 2>&1 || rc=1
			if ! [ $rc -eq 1 ]; then
				rm -r "$dir/$file"
			fi
		fi
	else
		echo "* Error - The User $user given not exists."
		exit 1
	fi
fi


exit 0
